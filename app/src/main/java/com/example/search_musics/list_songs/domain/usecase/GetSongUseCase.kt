package com.example.search_musics.list_songs.domain.usecase

import com.example.search_musics.list_songs.data.repository.SongRepository
import com.example.search_musics.list_songs.domain.model.Song
import com.example.search_musics.utils.UseCase
import io.reactivex.Observable
import javax.inject.Inject

class GetSongUseCase @Inject constructor(
        private val songRepository: SongRepository
) : UseCase<Song>() {

    private var page: Int = 0

    private lateinit var nameMusic : String

    fun setData(nameMusic:String,page: Int): GetSongUseCase {
        this.nameMusic = nameMusic;
        this.page = page
        return this
    }

    override fun createObservableUseCase(): Observable<Song> {
        return songRepository.getSong(nameMusic,page)
    }
}