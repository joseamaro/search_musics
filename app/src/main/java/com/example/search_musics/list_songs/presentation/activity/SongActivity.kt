package com.example.search_musics.list_songs.presentation.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.search_musics.R
import com.example.search_musics.list_songs.di.component.DaggerSongActivityComponent
import com.example.search_musics.list_songs.presentation.fragment.SongsListFragment
import javax.inject.Inject

class SongActivity : AppCompatActivity() {

    @Inject
    lateinit var songsListFragment: SongsListFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies()
        setContentView(R.layout.activity_main)
        setFragment(R.id.fragmentContainer, songsListFragment, "fragmentList")
    }

    private fun setFragment(idContainer: Int, fragment: Fragment, tag : String) {
        val trans: FragmentTransaction = supportFragmentManager.beginTransaction()
        trans.replace(idContainer, fragment, tag)
        trans.commit()
    }

    private  fun injectDependencies(){
        DaggerSongActivityComponent.builder().build().inject(this)
    }
}
