package com.example.search_musics.list_songs.di.module

import com.example.search_musics.list_songs.data.remote.SongApi
import com.example.search_musics.list_songs.data.repository.SongRepository
import com.example.search_musics.list_songs.data.repository.SongRepositoryImp
import com.example.search_musics.utils.ApiServiceFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient

@Module
class SongRepositoryModule {

    @Provides
    fun provideRepository(
            api: SongApi
    ): SongRepository {
        return SongRepositoryImp(api)
    }

    @Provides
    fun provideApiService(): SongApi {
        val okHttpClient = OkHttpClient()
        return ApiServiceFactory.build(
                okHttpClient,
                SongApi::class.java,
                "https://itunes.apple.com/"
        )
    }
}