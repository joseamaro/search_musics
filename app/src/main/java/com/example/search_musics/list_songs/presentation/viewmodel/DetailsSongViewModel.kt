package com.example.search_musics.list_songs.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.search_musics.list_songs.domain.model.Song
import com.example.search_musics.list_songs.domain.usecase.GetAlbumUseCase
import com.example.search_musics.utils.*
import javax.inject.Inject

class DetailsSongViewModel @Inject constructor(
        private val getAlbumUseCase: GetAlbumUseCase
) : ViewModel() {

    var liveData: MutableLiveData<Resource<Song>> = MutableLiveData()

    fun getAlbum(id: Int, song: String) {
        liveData.post(status = ResourceState.LOADING)
        getAlbumUseCase.setData(id,song).execute(object : UseCaseObserver<Song>() {

            override fun onNext(value: Song) {
                liveData.post(status = ResourceState.SUCCESS, data = value)
            }

            override fun onError(e: Throwable) {
                super.onError(e)
                liveData.post(status = ResourceState.ERROR,
                        failure = Failure.Error("No se encontró la búsqueda"))
            }
        })
    }

    override fun onCleared() {
        getAlbumUseCase.dispose()
    }
}