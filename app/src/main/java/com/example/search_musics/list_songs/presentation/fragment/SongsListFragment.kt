package com.example.search_musics.list_songs.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.search_musics.R
import com.example.search_musics.list_songs.di.component.DaggerSongsListFragmentComponent
import com.example.search_musics.list_songs.domain.model.Result
import com.example.search_musics.list_songs.domain.model.Song
import com.example.search_musics.list_songs.presentation.adapter.SongsListAdapter
import com.example.search_musics.list_songs.presentation.viewmodel.SongsListViewModel
import com.example.search_musics.utils.*
import com.example.search_musics.utils.extension.showMessage
import com.example.search_musics.utils.extension.showProgress
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_list_songs.*
import javax.inject.Inject


class SongsListFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    var detailsSongFragment: DetailsSongFragment = DetailsSongFragment()

    lateinit var viewModel: SongsListViewModel

    lateinit var songsListAdapter: SongsListAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependence()
        initViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                viewModel.getSong(query, 20)
                return false
            }

        })
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_list_songs, container, false)

    companion object {
        fun newInstance(): SongsListFragment {
            val instance = SongsListFragment()
            return instance
        }
    }

    fun handleSongList(status: ResourceState, data: Song?, failure: Failure?) {
        when (status) {
            ResourceState.LOADING -> {
                pbCoupon.showProgress(true, activity)
            }
            ResourceState.SUCCESS -> {
                pbCoupon.showProgress(false, activity)
                data?.let {
                    displaySong(it.results)
                }
            }
            ResourceState.ERROR -> {
                pbCoupon.showProgress(false, activity)
                showMessage((failure as Failure.Error).errorMessage, context)
            }
            else -> {
            }
        }

    }

    private fun injectDependence() {
        DaggerSongsListFragmentComponent.builder().build().inject(this)
    }

    private fun displaySong(results: List<Result>) {
        songsListAdapter = SongsListAdapter {
            changeFragment(it)
        }
        recycler_view.layoutManager = LinearLayoutManager(activity)
        recycler_view.layoutManager = GridLayoutManager(context, 1)
        recycler_view.adapter = songsListAdapter
        songsListAdapter.setList(results)
    }

    fun initViewModel() {
        viewModel = getViewModel(viewModelFactory)
        viewModel.liveData.observe(this, Observer {
            it?.also {
                handleSongList(it.status, it.data, it.failure)
            }
        })
    }

    private fun addFragment(fragment: Fragment?, idContainer: Int) {
        val trans: FragmentTransaction? = fragmentManager?.beginTransaction()
        fragment?.let {
            trans?.add(idContainer, it)
        }
        trans?.addToBackStack(null)
        trans?.commit()
    }

    private fun changeFragment(it: Result) {
        val b = Bundle()
        b.putString("song", Gson().toJson(it))
        detailsSongFragment.setArguments(b)
        addFragment(detailsSongFragment, R.id.fragmentContainer)
    }
}