package com.example.search_musics.list_songs.domain.usecase

import com.example.search_musics.list_songs.data.repository.SongRepository
import com.example.search_musics.list_songs.domain.model.Song
import com.example.search_musics.utils.UseCase
import com.example.search_musics.utils.UseCaseObserver
import io.reactivex.Observable
import javax.inject.Inject

class GetAlbumUseCase @Inject constructor(
        private val songRepository: SongRepository
) : UseCase<Song>() {

    private var id: Int = 0

    private lateinit var song : String

    fun setData(id:Int, song: String): GetAlbumUseCase {
        this.id = id;
        this.song = song
        return this
    }

    override fun createObservableUseCase(): Observable<Song> {
        return songRepository.getAlbum(id, song)
    }
}