package com.example.search_musics.list_songs.di.component

import com.example.search_musics.list_songs.di.module.DetailsSongFragmentModule
import com.example.search_musics.list_songs.di.module.SongRepositoryModule
import com.example.search_musics.list_songs.presentation.fragment.DetailsSongFragment
import com.example.search_musics.utils.FragmentComponent
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DetailsSongFragmentModule::class, SongRepositoryModule::class])
interface DetailsSongFragmentComponent : FragmentComponent<DetailsSongFragment> {
}