package com.example.search_musics.list_songs.di.module

import com.example.search_musics.list_songs.presentation.fragment.SongsListFragment
import dagger.Module
import dagger.Provides

@Module
class SongActivityModule {
    @Provides
    fun provideFragment(): SongsListFragment {
        return SongsListFragment.newInstance()
    }
}