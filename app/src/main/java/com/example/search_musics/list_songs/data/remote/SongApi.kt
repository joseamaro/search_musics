package com.example.search_musics.list_songs.data.remote

import com.example.search_musics.list_songs.domain.model.Song
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface SongApi {
    @GET("search?&mediaType=music")
    fun getSong(@Query("term") term: String,
                @Query("limit") limit: Int): Observable<Song>

    @GET("lookup")
    fun getAlbum(@Query("id") id: Int,
                @Query("entity") limit: String): Observable<Song>
}