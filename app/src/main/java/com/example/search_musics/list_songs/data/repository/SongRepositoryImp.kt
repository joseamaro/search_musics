package com.example.search_musics.list_songs.data.repository

import com.example.search_musics.list_songs.data.remote.SongApi
import com.example.search_musics.list_songs.domain.model.Song
import io.reactivex.Observable
import java.lang.Exception

class SongRepositoryImp(
        private val api: SongApi) : SongRepository {

    override fun getSong(nameMusic: String, page: Int): Observable<Song> {
        return api.getSong(nameMusic,page).map {
            if (it.results.isEmpty()) {
                throw Exception("Error")
            }
            it
        }

    }

    override fun getAlbum(id: Int, song: String): Observable<Song> {
        return api.getAlbum(id, song).map{
            if (it.results.isEmpty()) {
                throw Exception("Error")
            }
            it
        }
    }
}