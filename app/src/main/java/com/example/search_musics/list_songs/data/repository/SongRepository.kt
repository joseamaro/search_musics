package com.example.search_musics.list_songs.data.repository

import com.example.search_musics.list_songs.domain.model.Song
import io.reactivex.Observable

interface SongRepository {
    fun getSong(nameMusic: String, page: Int): Observable<Song>
    fun getAlbum(id: Int, song: String): Observable<Song>
}