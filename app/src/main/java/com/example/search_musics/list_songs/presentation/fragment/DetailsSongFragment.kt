package com.example.search_musics.list_songs.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.search_musics.R
import com.example.search_musics.list_songs.di.component.DaggerDetailsSongFragmentComponent
import com.example.search_musics.list_songs.domain.model.Result
import com.example.search_musics.list_songs.domain.model.Song
import com.example.search_musics.list_songs.presentation.adapter.DetailsSongAdapter
import com.example.search_musics.list_songs.presentation.viewmodel.DetailsSongViewModel
import com.example.search_musics.list_songs.presentation.viewmodel.SongsListViewModel
import com.example.search_musics.utils.Failure
import com.example.search_musics.utils.ResourceState
import com.example.search_musics.utils.ViewModelFactory
import com.example.search_musics.utils.extension.loadUrl
import com.example.search_musics.utils.extension.showMessage
import com.example.search_musics.utils.extension.showProgress
import com.example.search_musics.utils.getViewModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_details_song.*
import kotlinx.android.synthetic.main.fragment_list_songs.*
import kotlinx.android.synthetic.main.fragment_list_songs.recycler_view
import javax.inject.Inject

class DetailsSongFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    lateinit var viewModel: DetailsSongViewModel

    lateinit var detailsSongAdapter: DetailsSongAdapter

    private lateinit var result: Result

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependence()
        initViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            val jsonTransaction = it.getString("song")
            result = Gson().fromJson<Result>(jsonTransaction, Result::class.java)
            viewModel.getAlbum(result.collectionId, "song")
        }
        iv_back.setOnClickListener {
            fragmentManager?.popBackStack()
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_details_song, container, false)

    fun injectDependence() {
        DaggerDetailsSongFragmentComponent.builder().build().inject(this)
    }

    fun handleSongList(status: ResourceState, data: Song?, failure: Failure?) {
        when (status) {
            ResourceState.LOADING -> {
                pb_details.showProgress(true, activity)
            }
            ResourceState.SUCCESS -> {
                pb_details.showProgress(false, activity)
                data?.let {
                    displaySong(it.results)
                }
            }
            ResourceState.ERROR -> {
                pb_details.showProgress(false, activity)
                showMessage((failure as Failure.Error).errorMessage, context)
            }
            else -> {
            }
        }

    }

    private fun displaySong(results: List<Result>) {
        detailsSongAdapter = DetailsSongAdapter {}
        recycler_view.layoutManager = LinearLayoutManager(activity)
        recycler_view.layoutManager = GridLayoutManager(context, 1)
        recycler_view.adapter = detailsSongAdapter
        detailsSongAdapter.setList(results)
        tv_name_album.text = results[0].collectionName
        tv_name_artist.text = results[0].artistName
        image.loadUrl(results[0].artworkUrl100)
    }

    fun initViewModel() {
        viewModel = getViewModel(viewModelFactory)
        viewModel.liveData.observe(this, Observer {
            it?.also {
                handleSongList(it.status, it.data, it.failure)
            }
        })
    }
}