package com.example.search_musics.list_songs.di.component

import com.example.search_musics.list_songs.di.module.SongRepositoryModule
import com.example.search_musics.list_songs.di.module.SongsListFragmentModule
import com.example.search_musics.list_songs.presentation.fragment.SongsListFragment
import com.example.search_musics.utils.FragmentComponent
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [SongsListFragmentModule::class, SongRepositoryModule::class])
interface SongsListFragmentComponent : FragmentComponent<SongsListFragment>{
}