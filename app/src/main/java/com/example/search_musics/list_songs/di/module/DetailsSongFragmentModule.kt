package com.example.search_musics.list_songs.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.search_musics.list_songs.presentation.viewmodel.DetailsSongViewModel
import com.example.search_musics.utils.ViewModelFactory
import com.example.search_musics.utils.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DetailsSongFragmentModule {

    @Binds
    @IntoMap
    @ViewModelKey(DetailsSongViewModel::class)
    abstract fun bindCompaniesViewModel(viewModel: DetailsSongViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}