package com.example.search_musics.list_songs.domain.model

class Song(val resultCount: Int,
           val results: List<Result>)
