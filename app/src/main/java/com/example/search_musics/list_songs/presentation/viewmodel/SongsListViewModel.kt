package com.example.search_musics.list_songs.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.search_musics.list_songs.domain.model.Song
import com.example.search_musics.list_songs.domain.usecase.GetSongUseCase
import com.example.search_musics.utils.*
import javax.inject.Inject

class SongsListViewModel @Inject constructor(
        private val getSongUseCase: GetSongUseCase
) : ViewModel() {

    var liveData: MutableLiveData<Resource<Song>> = MutableLiveData()


    fun getSong(nameMusic: String, page : Int) {
        liveData.post(status = ResourceState.LOADING)
        getSongUseCase.setData(nameMusic,page).execute(object : UseCaseObserver<Song>() {

            override fun onNext(value: Song) {
                liveData.post(status = ResourceState.SUCCESS, data = value)
            }

            override fun onError(e: Throwable) {
                liveData.post(status = ResourceState.ERROR,
                        failure = Failure.Error("No se encontró la búsqueda"))
            }
        })
    }

    override fun onCleared() {
        getSongUseCase.dispose()
    }
}