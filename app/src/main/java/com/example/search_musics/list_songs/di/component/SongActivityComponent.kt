package com.example.search_musics.list_songs.di.component

import com.example.search_musics.list_songs.di.module.SongActivityModule
import com.example.search_musics.list_songs.presentation.activity.SongActivity
import com.example.search_musics.utils.ActivityComponent
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [SongActivityModule::class])
interface SongActivityComponent : ActivityComponent<SongActivity>{
}