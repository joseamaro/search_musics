package com.example.search_musics.list_songs.presentation.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.search_musics.R
import com.example.search_musics.list_songs.domain.model.Result
import com.example.search_musics.utils.extension.inflate
import kotlinx.android.synthetic.main.item_list_songs.view.*

class DetailsSongAdapter constructor(val listener: (Result) -> Unit) :
        RecyclerView.Adapter<DetailsSongAdapter.ViewHolder>() {

    private lateinit var results: List<Result>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailsSongAdapter.ViewHolder {
        return ViewHolder(
                parent.inflate(R.layout.item_song)
        )
    }

    override fun getItemCount(): Int {
        return results.size
    }

    override fun onBindViewHolder(holder: DetailsSongAdapter.ViewHolder, position: Int) {
        holder.itemView.tv_song.text = results[position].trackName
    }

    fun setList(results: List<Result>) {
        this.results = results
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }
}